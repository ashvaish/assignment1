#!/bin/sh

#Author: Vaishnavi
#Copyright: Excelfore Copyright 2021
#echo "choose operation (+,-,*,/,%) : $1"
#echo "input first no. : $2"
#echo "input second no. : $3"

case $1 in
  +)res=`echo $2 + $3 | bc`
  ;;
  -)res=`echo $2 - $3 | bc`
  ;;
  *)res=`echo $2 * $3 | bc`
  ;;
  /)res=`echo $2 / $3 | bc`
  ;;
  %)res=`echo $2 % $3 | bc`
  ;;
esac
echo "Result : $res"
